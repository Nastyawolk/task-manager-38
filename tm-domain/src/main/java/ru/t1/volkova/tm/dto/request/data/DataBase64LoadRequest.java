package ru.t1.volkova.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.request.AbstractUserRequest;

public final class DataBase64LoadRequest extends AbstractUserRequest {

    public DataBase64LoadRequest(@Nullable final String token) {
        super(token);
    }

}
