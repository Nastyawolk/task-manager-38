package ru.t1.volkova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.model.Task;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    Connection getConnection();

    void update(@NotNull Task task) throws SQLException;

    @Nullable
    List<Task> findAllByProjectId(@Nullable String userId, @NotNull String projectId) throws SQLException;


}
