package ru.t1.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.IUserOwnedRepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.IUserOwnedService;
import ru.t1.volkova.tm.exception.field.IdEmptyException;
import ru.t1.volkova.tm.exception.field.IndexIncorrectException;
import ru.t1.volkova.tm.exception.field.UserIdEmptyException;
import ru.t1.volkova.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @NotNull
    protected Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    protected abstract IUserOwnedRepository<M> getRepository(@NotNull final Connection connection);

    @Override
    public void removeAll(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            repository.removeAll(userId);
            connection.commit();
        }
        catch (Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.existsById(userId, id);
        }
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findAll(userId);
        }
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            if (comparator == null) return repository.findAll(userId);
            return repository.findAll(userId, comparator);
        }
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findOneById(userId, id);
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findOneByIndex(userId, index);
        }
    }

    @Override
    public int getSize(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.getSize(userId);
        }
    }

    @Nullable
    @Override
    public M removeOneById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            @Nullable final M model = repository.removeOneById(userId, id);
            repository.removeAll(userId);
            connection.commit();
            return model;
        }
        catch (Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            @Nullable final M model = repository.removeOneByIndex(userId, index);
            repository.removeAll(userId);
            connection.commit();
            return model;
        }
        catch (Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            repository.add(userId, model);
            connection.commit();
            return model;
        }
        catch (Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public M removeOne(@Nullable final String userId, @Nullable final M model) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            repository.removeOne(userId, model);
            connection.commit();
        }
        catch (Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return model;
    }

}
