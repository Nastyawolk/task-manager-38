package ru.t1.volkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.DBConstants;
import ru.t1.volkova.tm.api.repository.IProjectRepository;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.model.Project;

import java.sql.*;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    protected String getTableName() {
        return DBConstants.TABLE_PROJECT;
    }

    @Override
    @NotNull
    public Connection getConnection() {
        return connection;
    }

    @Override
    @NotNull
    public Project fetch(@NotNull ResultSet row) throws SQLException {
        @NotNull final Project project = new Project();
        @Nullable final Status status = Status.toStatus(row.getString(DBConstants.COLUMN_STATUS));
        project.setId(row.getString(DBConstants.COLUMN_ID));
        project.setName(row.getString(DBConstants.COLUMN_NAME));
        project.setCreated(row.getTimestamp(DBConstants.COLUMN_CREATED));
        project.setDescription(row.getString(DBConstants.COLUMN_DESCRIPTION));
        project.setUserId(row.getString(DBConstants.COLUMN_USER_ID));
        project.setStatus(status == null ? Status.NOT_STARTED : status);
        return project;
    }

    @Override
    public void update(@NotNull final Project project) throws SQLException {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstants.COLUMN_NAME, DBConstants.COLUMN_CREATED,
                DBConstants.COLUMN_DESCRIPTION, DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_STATUS,
                DBConstants.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getName());
            statement.setTimestamp(2, new Timestamp(project.getCreated().getTime()));
            statement.setString(3, project.getDescription());
            statement.setString(4, project.getUserId());
            statement.setString(5, project.getStatus().getDisplayname());
            statement.setString(6, project.getId());
            statement.executeUpdate();
        }
    }

    @Override
    @NotNull
    public Project add(@NotNull final Project project) throws SQLException {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_NAME, DBConstants.COLUMN_CREATED,
                DBConstants.COLUMN_DESCRIPTION, DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_STATUS
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getId());
            statement.setString(2, project.getName());
            statement.setTimestamp(3, new Timestamp(project.getCreated().getTime()));
            statement.setString(4, project.getDescription());
            statement.setString(5, project.getUserId());
            statement.setString(6, project.getStatus().getDisplayname());
            statement.executeUpdate();
        }
        return project;
    }

    @Override
    @NotNull
    public Project add(
            @Nullable final String userId,
            @NotNull final Project project) throws SQLException {
        project.setUserId(userId);
        return add(project);
    }

}
