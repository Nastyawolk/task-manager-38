package ru.t1.volkova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.model.Project;

import java.sql.Connection;
import java.sql.SQLException;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull Connection getConnection();

    void update(@NotNull Project project) throws SQLException;
}
