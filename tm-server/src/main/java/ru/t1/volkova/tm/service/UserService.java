package ru.t1.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.IProjectRepository;
import ru.t1.volkova.tm.api.repository.ITaskRepository;
import ru.t1.volkova.tm.api.repository.IUserRepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.IPropertyService;
import ru.t1.volkova.tm.api.service.IUserService;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.exception.entity.UserNotFoundException;
import ru.t1.volkova.tm.exception.field.EmailEmptyException;
import ru.t1.volkova.tm.exception.field.IdEmptyException;
import ru.t1.volkova.tm.exception.field.LoginEmptyException;
import ru.t1.volkova.tm.exception.field.PasswordEmptyException;
import ru.t1.volkova.tm.exception.user.EmailExistsException;
import ru.t1.volkova.tm.exception.user.LoginExistsException;
import ru.t1.volkova.tm.exception.user.RoleEmptyException;
import ru.t1.volkova.tm.model.User;
import ru.t1.volkova.tm.repository.UserRepository;
import ru.t1.volkova.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    @Override
    protected @NotNull IUserRepository getRepository(@NotNull final Connection connection) {
        return new UserRepository(connection);
    }

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IProjectRepository projectRepository,
            @NotNull final ITaskRepository taskRepository,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        @NotNull final Connection connection = getConnection();
        try {
            add(user);
            connection.commit();
        }
        catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new EmailExistsException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        user.setEmail(email);
        @NotNull final Connection connection = getConnection();
        try {
            add(user);
            connection.commit();
        }
        catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        user.setRole(role);
        @NotNull final Connection connection = getConnection();
        try {
            add(user);
            connection.commit();
        }
        catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.findOneByLogin(login);
        }
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) throws SQLException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.findOneByEmail(email);
        }
    }

    @Nullable
    @Override
    public User removeOne(@Nullable final User model) throws Exception {
        if (model == null) return null;
        @Nullable final User user = super.removeOne(model);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        taskRepository.removeAll(userId);
        projectRepository.removeAll(userId);
        return user;
    }

    @Nullable
    @Override
    public User removeByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        return this.removeOne(user);
    }

    @Nullable
    @Override
    public User removeByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = findByEmail(email);
        return this.removeOne(user);
    }

    @NotNull
    @Override
    public User setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            @Nullable final User user = repository.findOneById(id);
            if (user == null) throw new UserNotFoundException();
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            repository.update(user);
            connection.commit();
            return user;
        }
        catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            @Nullable final User user = repository.findOneById(id);
            if (user == null) throw new UserNotFoundException();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            repository.update(user);
            connection.commit();
            return user;
        }
        catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) return false;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.isLoginExist(login);
        }
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) throws SQLException {
        if (email == null || email.isEmpty()) return false;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.isEmailExist(email);
        }
    }

    @NotNull
    @Override
    public User lockUserByLogin(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            @Nullable final User user = findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(true);
            repository.update(user);
            connection.commit();
            return user;
        }
        catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public User unlockUserByLogin(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            @Nullable final User user = findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(false);
            repository.update(user);
            connection.commit();
            return user;
        }
        catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
    }

}
