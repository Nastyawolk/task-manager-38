package ru.t1.volkova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.DBConstants;
import ru.t1.volkova.tm.api.repository.ITaskRepository;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.model.Task;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    public Connection getConnection() {
        return connection;
    }

    @Override
    @NotNull
    protected String getTableName() {
        return DBConstants.TABLE_TASK;
    }

    @NotNull
    public Task fetch(@NotNull final ResultSet row) throws SQLException {
        @NotNull final Task task = new Task();
        @Nullable final Status status = Status.toStatus(row.getString(DBConstants.COLUMN_STATUS));
        task.setId(row.getString(DBConstants.COLUMN_ID));
        task.setName(row.getString(DBConstants.COLUMN_NAME));
        task.setCreated(row.getTimestamp(DBConstants.COLUMN_CREATED));
        task.setDescription(row.getString(DBConstants.COLUMN_DESCRIPTION));
        task.setUserId(row.getString(DBConstants.COLUMN_USER_ID));
        task.setStatus(status == null ? Status.NOT_STARTED : status);
        task.setProjectId(row.getString(DBConstants.COLUMN_PROJECT_ID));
        return task;
    }

    @Override
    public void update(@NotNull final Task task) throws SQLException {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstants.COLUMN_NAME, DBConstants.COLUMN_CREATED,
                DBConstants.COLUMN_DESCRIPTION, DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_STATUS,
                DBConstants.COLUMN_PROJECT_ID, DBConstants.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setTimestamp(2, new Timestamp(task.getCreated().getTime()));
            statement.setString(3, task.getDescription());
            statement.setString(4, task.getUserId());
            statement.setString(5, task.getStatus().getDisplayname());
            statement.setString(6, task.getProjectId());
            statement.setString(7, task.getId());
            statement.executeUpdate();
        }
    }

    @NotNull
    public Task add(@NotNull final Task task) throws SQLException {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_NAME, DBConstants.COLUMN_CREATED,
                DBConstants.COLUMN_DESCRIPTION, DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_STATUS,
                DBConstants.COLUMN_PROJECT_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getId());
            statement.setString(2, task.getName());
            statement.setTimestamp(3, new Timestamp(task.getCreated().getTime()));
            statement.setString(4, task.getDescription());
            statement.setString(5, task.getUserId());
            statement.setString(6, task.getStatus().getDisplayname());
            statement.setString(7, task.getProjectId());
            statement.executeUpdate();
        }
        return task;
    }

    @Override
    public @NotNull Task add(
            @Nullable final String userId,
            @NotNull final Task task
    ) throws SQLException {
        task.setUserId(userId);
        return add(task);
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @NotNull final String projectId) throws SQLException {
        if (userId == null) return null;
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? AND %s = ?", getTableName(), DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_PROJECT_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

}
