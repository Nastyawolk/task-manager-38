package ru.t1.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.IProjectRepository;
import ru.t1.volkova.tm.api.repository.IUserOwnedRepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.IProjectService;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.field.DescriptionEmptyException;
import ru.t1.volkova.tm.exception.field.IdEmptyException;
import ru.t1.volkova.tm.exception.field.IndexIncorrectException;
import ru.t1.volkova.tm.exception.field.NameEmptyException;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.sql.SQLException;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository>
        implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    protected @NotNull IProjectRepository getRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    @Nullable
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description) throws SQLException {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.add(userId, project);
            connection.commit();
        }
        catch (@NotNull Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
        }
        catch (@NotNull Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws SQLException {
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
        }
        catch (@NotNull Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        project.setStatus(status);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
        }
        catch (@NotNull Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) throws SQLException {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @Nullable final Project project = findOneByIndex(userId, index);
        project.setStatus(status);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
        }
        catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project findOneById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        @Nullable final Project project = super.findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @NotNull
    @Override
    public Project findOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException {
        @Nullable final Project project = super.findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

}
