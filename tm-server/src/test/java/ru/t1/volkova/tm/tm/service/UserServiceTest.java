package ru.t1.volkova.tm.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.IUserService;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.exception.entity.UserNotFoundException;
import ru.t1.volkova.tm.exception.field.EmailEmptyException;
import ru.t1.volkova.tm.exception.field.IdEmptyException;
import ru.t1.volkova.tm.exception.field.LoginEmptyException;
import ru.t1.volkova.tm.exception.field.PasswordEmptyException;
import ru.t1.volkova.tm.exception.user.LoginExistsException;
import ru.t1.volkova.tm.exception.user.RoleEmptyException;
import ru.t1.volkova.tm.model.User;
import ru.t1.volkova.tm.repository.ProjectRepository;
import ru.t1.volkova.tm.repository.TaskRepository;
import ru.t1.volkova.tm.repository.UserRepository;
import ru.t1.volkova.tm.service.ConnectionService;
import ru.t1.volkova.tm.service.PropertyService;
import ru.t1.volkova.tm.service.UserService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class UserServiceTest {

    private static final int NUMBER_OF_ENTRIES = 4;

    @NotNull
    private List<User> userList;

    @NotNull
    private IUserService userService;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Before
    public void initRepository() throws SQLException {
        userList = new ArrayList<>();
        userService = new UserService
                (
                        connectionService,
                        new ProjectRepository(connectionService.getConnection()),
                        new TaskRepository(connectionService.getConnection()),
                        propertyService
                );

        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("user" + i);
            user.setEmail("user@" + i + ".ru");
            user.setRole(Role.USUAL);
            userService.add(user);
            userList.add(user);
        }
    }

    @Test
    public void testCreate(
    ) throws Exception {
        @Nullable final User user = userService.create("new_login", "password332");
        Assert.assertEquals(user, userService.findOneById(user.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, userService.getSize().intValue());
    }

    @Test
    public void testCreateEmail(
    ) throws Exception {
        @Nullable final User user = userService.create("new_login", "password332", "new_user@mail.ru");
        Assert.assertEquals(user, userService.findOneById(user.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, userService.getSize().intValue());
    }

    @Test
    public void testCreateRole(
    ) throws Exception {
        @Nullable final User user = userService.create("admin2", "password332", Role.ADMIN);
        Assert.assertEquals(user, userService.findOneById(user.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, userService.getSize().intValue());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test(expected = LoginEmptyException.class)
    public void testCreateLoginEmpty(
    ) throws SQLException {
        userService.create(null, "password332");
        userService.create("", "password332");
    }

    @Test(expected = LoginExistsException.class)
    public void testCreateLoginExists(
    ) throws SQLException {
        userService.create("user2", "password332");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testCreatePasswordEmpty(
    ) throws SQLException {
        userService.create("new_user", null);
        userService.create("new_user", "");
    }

    @Test(expected = EmailEmptyException.class)
    public void testCreateEmailEmpty(
    ) throws SQLException {
        userService.create("new_login", "password332", "");
        userService.create("new_login", "password332", (String) null);
    }

    @Test(expected = RoleEmptyException.class)
    public void testCreateRoleEmpty(
    ) throws SQLException {
        userService.create("new_login", "password332", (Role) null);
    }

    @Test
    public void testFindByLogin() throws SQLException {
        Assert.assertEquals(userList.get(1), userService.findByLogin("user2"));
        Assert.assertEquals(userList.get(0), userService.findByLogin("user1"));
    }

    @Test
    public void testFindByLoginNegative() throws SQLException {
        Assert.assertNull(userService.findByLogin("non-existent"));
    }

    @Test(expected = LoginEmptyException.class)
    public void testFindByLoginEmpty() throws SQLException {
        Assert.assertEquals(userList.get(1), userService.findByLogin(""));
        Assert.assertEquals(userList.get(0), userService.findByLogin(null));
    }

    @Test
    public void testFindByEmail() throws SQLException {
        Assert.assertEquals(userList.get(1), userService.findByEmail("user@2.ru"));
        Assert.assertEquals(userList.get(0), userService.findByEmail("user@1.ru"));
    }

    @Test
    public void testFindByEmailNegative() throws SQLException {
        Assert.assertNull(userService.findByEmail("non-existent"));
    }

    @Test(expected = EmailEmptyException.class)
    public void testFindByEmailEmpty() throws SQLException {
        Assert.assertEquals(userList.get(1), userService.findByEmail(""));
        Assert.assertEquals(userList.get(0), userService.findByEmail(null));
    }

    @Test
    public void testRemoveOne() throws Exception {
        Assert.assertEquals(userList.get(1), userService.removeOne(userList.get(1)));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, userService.getSize().intValue());
    }

    @Test
    public void testRemoveOneNegative() throws Exception {
        Assert.assertNull(userService.removeOne(null));
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveOneUserNotFound() throws Exception {
        userService.removeOne(new User());
    }

    @Test
    public void testRemoveByLogin() throws Exception {
        Assert.assertEquals(userList.get(1), userService.removeByLogin(userList.get(1).getLogin()));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, userService.getSize().intValue());
    }

    @Test(expected = LoginEmptyException.class)
    public void testRemoveByLoginEmptyLogin() throws Exception {
        userService.removeByLogin(null);
        userService.removeByLogin("");
    }

    @Test
    public void testRemoveByEmail() throws Exception {
        Assert.assertEquals(userList.get(1), userService.removeByEmail(userList.get(1).getEmail()));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, userService.getSize().intValue());
    }

    @Test(expected = EmailEmptyException.class)
    public void testRemoveByLoginEmptyEmail() throws Exception {
        userService.removeByEmail(null);
        userService.removeByEmail("");
    }

    @Test
    public void testSetPassword() throws Exception {
        Assert.assertNotNull(userService.setPassword(userList.get(0).getId(), "newPass"));
    }

    @Test(expected = IdEmptyException.class)
    public void testSetPasswordEmptyId() throws Exception {
        userService.setPassword(null, "newPass");
        userService.setPassword("", "newPass");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testSetPasswordEmptyPassword() throws Exception {
        userService.setPassword(userList.get(1).getId(), "");
        userService.setPassword(userList.get(1).getId(), null);
    }

    @Test
    public void testUpdateUser() throws Exception {
        @NotNull final String firstName = "new name";
        @NotNull final String lastName = "new lastName";
        @NotNull final String middleName = "new middleName";
        @NotNull final String id = userList.get(0).getId();
        userService.updateUser(id, firstName, lastName, middleName);
        @Nullable final User user = userService.findOneById(id);
        if (user == null) return;
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());
    }

    @Test(expected = UserNotFoundException.class)
    public void testUpdateNotFoundTask(
    ) throws Exception {
        @NotNull final String firstName = "new name";
        @NotNull final String lastName = "new lastName";
        @NotNull final String middleName = "new middleName";
        userService.updateUser("non-existent", firstName, lastName, middleName);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateIdEmpty(
    ) throws Exception {
        userService.updateUser("", "firstName", "lastName", "middleName");
        userService.updateUser(null, "firstName", "lastName", "middleName");
    }

    @Test
    public void testIsLoginExist() throws SQLException {
        Assert.assertTrue(userService.isLoginExist("user4"));
    }

    @Test
    public void testIsLoginExistNegative() throws SQLException {
        Assert.assertFalse(userService.isLoginExist("user4444"));
        Assert.assertFalse(userService.isLoginExist(""));
        Assert.assertFalse(userService.isLoginExist(null));
    }

    @Test
    public void testIsEmailExist() throws SQLException {
        Assert.assertTrue(userService.isEmailExist("user@4.ru"));
    }

    @Test
    public void testLockUserByLogin() throws SQLException {
        @NotNull final User user = userService.lockUserByLogin(userList.get(0).getLogin());
        Assert.assertTrue(user.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    public void testLockUserByEmptyLogin() throws SQLException {
        userService.lockUserByLogin("");
        userService.lockUserByLogin(null);
    }

    @Test(expected = UserNotFoundException.class)
    public void testLockUserByEmptyId() throws SQLException {
        userService.lockUserByLogin("non-existent");
    }

    @Test
    public void testUnlockUserByLogin() throws SQLException {
        @NotNull final User user = userService.unlockUserByLogin(userList.get(0).getLogin());
        Assert.assertFalse(user.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    public void testUnlockUserByEmptyLogin() throws SQLException {
        userService.unlockUserByLogin("");
        userService.unlockUserByLogin(null);
    }

    @Test(expected = UserNotFoundException.class)
    public void testUnlockUserByEmptyId() throws SQLException {
        userService.unlockUserByLogin("non-existent");
    }

    @Test
    public void testFindOneById() throws Exception {
        Assert.assertNotNull(userService.findOneById(userList.get(0).getId()));
        Assert.assertNotNull(userService.findOneById(userList.get(2).getId()));
    }

    @Test
    public void testFindOneByIdNegative() throws Exception {
        Assert.assertNull(userService.findOneById("non-existent"));
    }

}
