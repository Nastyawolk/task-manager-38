package ru.t1.volkova.tm.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.repository.IProjectRepository;
import ru.t1.volkova.tm.api.repository.ITaskRepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.IProjectTaskService;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.entity.TaskNotFoundException;
import ru.t1.volkova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.volkova.tm.exception.field.TaskIdEmptyException;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.model.Task;
import ru.t1.volkova.tm.repository.ProjectRepository;
import ru.t1.volkova.tm.repository.TaskRepository;
import ru.t1.volkova.tm.service.ConnectionService;
import ru.t1.volkova.tm.service.ProjectTaskService;
import ru.t1.volkova.tm.service.PropertyService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProjectTaskTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private List<Project> projectList;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private IProjectTaskService projectTaskService;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Before
    public void initRepository() throws SQLException {
        projectList = new ArrayList<>();
        taskRepository = new TaskRepository(connectionService.getConnection());
        projectRepository = new ProjectRepository(connectionService.getConnection());
        projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        @NotNull final Project project = new Project();
        @NotNull final Project project2 = new Project();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull Task task = new Task();
            task.setName("task" + i);
            task.setDescription("description" + i);
            if (i <= 5) {
                task.setUserId(USER_ID_1);
                project.setUserId(USER_ID_1);
                task.setProjectId(project.getId());
            } else {
                task.setUserId(USER_ID_2);
                project2.setUserId(USER_ID_2);
                task.setProjectId(project2.getId());
            }
            taskRepository.add(task);
        }
        projectRepository.add(project);
        projectRepository.add(project2);
        projectList.add(project);
        projectList.add(project2);
    }

    @Test
    public void testBindTaskToProject() throws Exception {
        @Nullable final Task task = taskRepository.add(new Task());
        if (task == null) return;
        task.setUserId(USER_ID_1);
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final String taskId = task.getId();
        @NotNull final Task bindedTask = projectTaskService.bindTaskToProject(USER_ID_1, projectId, taskId);
        Assert.assertEquals(task, bindedTask);
        Assert.assertEquals(taskRepository.findOneById(USER_ID_1, taskId), bindedTask);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testBindTaskWithEmptyProjectId() throws Exception {
        @Nullable final Task task = taskRepository.add(new Task());
        if (task == null) return;
        task.setUserId(USER_ID_1);
        @NotNull final String taskId = task.getId();
        projectTaskService.bindTaskToProject(USER_ID_1, null, taskId);
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testBindTaskWithEmptyTaskId() throws Exception {
        @Nullable final Task task = taskRepository.add(new Task());
        if (task == null) return;
        task.setUserId(USER_ID_1);
        @NotNull final String projectId = projectList.get(0).getId();
        projectTaskService.bindTaskToProject(USER_ID_1, projectId, null);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testBindTaskWithTaskNotFound() throws Exception {
        @Nullable final Task task = taskRepository.add(new Task());
        if (task == null) return;
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final String taskId = task.getId();
        projectTaskService.bindTaskToProject(USER_ID_1, projectId, taskId);
        projectTaskService.bindTaskToProject(USER_ID_1, projectId, taskId);
    }

    @Test
    public void testRemoveByProjectId() throws SQLException {
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final String projectId2 = projectList.get(1).getId();
        projectTaskService.removeProjectById(USER_ID_1, projectId);
        projectTaskService.removeProjectById(USER_ID_2, projectId2);
        @Nullable final List<Task> tasks = taskRepository.findAllByProjectId(USER_ID_1, projectId);
        @Nullable final List<Task> tasks2 = taskRepository.findAllByProjectId(USER_ID_2, projectId);
        if (tasks == null || tasks2 == null) return;
        Assert.assertEquals(0, tasks.size());
        Assert.assertEquals(0, tasks2.size());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testRemoveByEmptyProjectId() throws SQLException {
        projectTaskService.removeProjectById(USER_ID_1, null);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testRemoveByNonExistProjectId() throws SQLException {
        projectTaskService.removeProjectById(USER_ID_1, "NonExist");
    }

    @Test(expected = TaskNotFoundException.class)
    public void testRemoveByProjectIdTaskException() throws SQLException {
        @Nullable final Project project = new Project();
        projectRepository.add(USER_ID_1, project);
        projectTaskService.removeProjectById(USER_ID_1, project.getId());
    }

    @Test
    public void testUnbindTaskToProject() throws SQLException {
        @Nullable final Task task = taskRepository.add(new Task());
        if (task == null) return;
        task.setUserId(USER_ID_1);
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final String taskId = task.getId();
        task.setProjectId(projectId);
        projectTaskService.unbindTaskFromProject(USER_ID_1, projectId, taskId);
        Assert.assertNotEquals(task.getProjectId(), projectId);
        Assert.assertNull(task.getProjectId());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testUnbindTaskWithEmptyProjectId() throws SQLException {
        @Nullable final Task task = taskRepository.add(new Task());
        if (task == null) return;
        task.setUserId(USER_ID_1);
        @NotNull final String taskId = task.getId();
        projectTaskService.unbindTaskFromProject(USER_ID_1, null, taskId);
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testUnbindTaskWithEmptyTaskId() throws SQLException {
        @Nullable final Task task = taskRepository.add(new Task());
        if (task == null) return;
        task.setUserId(USER_ID_1);
        @NotNull final String projectId = projectList.get(0).getId();
        projectTaskService.unbindTaskFromProject(USER_ID_1, projectId, null);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUnbindTaskWithTaskNotFound() throws SQLException {
        @Nullable final Task task = taskRepository.add(new Task());
        if (task == null) return;
        @NotNull final String projectId = projectList.get(0).getId();
        @NotNull final String taskId = task.getId();
        projectTaskService.unbindTaskFromProject(USER_ID_1, projectId, taskId);
    }

}
