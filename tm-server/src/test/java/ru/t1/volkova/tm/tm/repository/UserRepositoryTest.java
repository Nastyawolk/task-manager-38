package ru.t1.volkova.tm.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.repository.IUserRepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.model.User;
import ru.t1.volkova.tm.repository.UserRepository;
import ru.t1.volkova.tm.service.ConnectionService;
import ru.t1.volkova.tm.service.PropertyService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 4;

    @NotNull
    private List<User> userList;

    @NotNull
    private IUserRepository userRepository;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Before
    public void initRepository() throws SQLException {
        userList = new ArrayList<>();
        userRepository = new UserRepository(connectionService.getConnection());
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull User user = new User();
            user.setLogin("user" + i);
            user.setEmail("user@" + i +".ru");
            user.setRole(Role.USUAL);
            userRepository.add(user);
            userList.add(user);
        }
    }

    @Test
    public void testAddUser() throws SQLException {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final User newUser = new User();
        userRepository.add(newUser);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testAddAll() throws SQLException {
        final int recordsCount = NUMBER_OF_ENTRIES / 2;
        final Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + recordsCount;
        @NotNull final List<User> userList = new ArrayList<>();
        for (int i = 0; i < recordsCount; i++) {
            userList.add(new User());
        }
        userRepository.add(userList);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testSet() throws Exception {
        final int recordsCount = NUMBER_OF_ENTRIES / 2;
        final Integer expectedNumberOfEntries = recordsCount;
        final int oldUserSize = userRepository.findAll().size();
        @NotNull final List<User> userList = new ArrayList<>();
        for (int i = 0; i < recordsCount; i++) {
            userList.add(new User());
        }
        userRepository.set(userList);
        final int newUserSize = userRepository.findAll().size();
        Assert.assertNotEquals(newUserSize, oldUserSize);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final List<User> userList = userRepository.findAll();
        Assert.assertEquals(NUMBER_OF_ENTRIES, userList.size());
    }

    @Test
    public void testFindOneById() throws Exception {
        @Nullable final String userId1 = userRepository.findOneByIndex(1).getId();
        @Nullable final String userId2 = userRepository.findOneByIndex(2).getId();
        @NotNull final User expected1 = userList.get(1);
        @NotNull final User expected2 = userList.get(2);
        Assert.assertEquals(expected1, userRepository.findOneById(userId1));
        Assert.assertEquals(expected2, userRepository.findOneById(userId2));
    }

    @Test
    public void testFindOneByIdNegative() throws Exception {
        Assert.assertNull(userRepository.findOneById("NotExcitingId"));
    }

    @Test
    public void testFindOneByIndex() throws Exception {
        @Nullable final User user1 = userRepository.findOneByIndex(1);
        @Nullable final User user2 = userRepository.findOneByIndex(2);
        @NotNull final User expected1 = userList.get(1);
        @NotNull final User expected2 = userList.get(2);
        Assert.assertEquals(expected1, user1);
        Assert.assertEquals(expected2, user2);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testFindOneByIndexNegative() throws Exception {
        Assert.assertNotNull(userRepository.findOneByIndex(NUMBER_OF_ENTRIES + 20));
        Assert.assertNotNull(userRepository.findOneByIndex(NUMBER_OF_ENTRIES * 2));
    }

    @Test
    public void testFindOneByIndexForUser() throws Exception {
        @Nullable final User user1 = userRepository.findOneByIndex(1);
        @Nullable final User user2 = userRepository.findOneByIndex(2);
        @NotNull final User expected1 = userList.get(1);
        @NotNull final User expected2 = userList.get(2);
        Assert.assertEquals(expected1, user1);
        Assert.assertEquals(expected2, user2);
    }

    @Test
    public void testRemoveOne() throws Exception {
        @Nullable final User user1 = userRepository.findOneByIndex(0);
        @Nullable final User user2 = userRepository.findOneByIndex(1);
        Assert.assertEquals(userList.get(0),userRepository.removeOne(user1));
        Assert.assertEquals(userList.get(1),userRepository.removeOne(user2));
    }

    @Test
    public void testRemoveOneNegative() throws Exception {
        @NotNull final User user1 = new User();
        @NotNull final User user2 = new User();
        Assert.assertNull(userRepository.removeOne(user1));
        Assert.assertNull(userRepository.removeOne(user2));
    }

    @Test
    public void testRemoveOneById() throws Exception {
        @Nullable final String userId1 = userRepository.findOneByIndex(0).getId();
        @Nullable final String userId2 = userRepository.findOneByIndex(3).getId();
        @NotNull final User expected1 = userList.get(0);
        @NotNull final User expected2 = userList.get(3);
        Assert.assertEquals(expected1, userRepository.removeOneById(userId1));
        Assert.assertEquals(expected2, userRepository.removeOneById(userId2));

        Assert.assertNotEquals(expected2, userRepository.removeOneById(userId1));
        Assert.assertNotEquals(expected1, userRepository.removeOneById(userId2));
    }

    @Test
    public void testRemoveOneByIdNegative() throws Exception {
        @NotNull final String userId1 = UUID.randomUUID().toString();
        @NotNull final String userId2 = UUID.randomUUID().toString();
        Assert.assertNull(userRepository.removeOneById(userId1));
        Assert.assertNull(userRepository.removeOneById(userId2));
    }

    @Test
    public void testRemoveAll() throws SQLException {
        userRepository.removeAll();
        Assert.assertEquals(0, userRepository.getSize().intValue());
    }

    @Test
    public void testRemoveOneByIndex() throws Exception {
        @Nullable final User user1 = userRepository.removeOneByIndex(1);
        Assert.assertEquals(NUMBER_OF_ENTRIES-1, userRepository.getSize().intValue());
        @Nullable final User user2 = userRepository.removeOneByIndex(2);
        Assert.assertEquals(NUMBER_OF_ENTRIES-2, userRepository.getSize().intValue());
        if (user1 != null && user2 != null) {
            Assert.assertNull(userRepository.findOneById(user1.getId()));
            Assert.assertNull(userRepository.findOneById(user2.getId()));
        }
    }

    @Test
    public void testRemoveOneByIndexNegative() throws Exception {
        Assert.assertNull(userRepository.removeOneByIndex(null));
    }

    @Test
    public void testGetSize() throws SQLException {
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize().intValue());
        Assert.assertEquals(userList.size(), userRepository.getSize().intValue());
    }

    @Test
    public void testFindByLogin() throws SQLException {
        Assert.assertEquals(userList.get(1), userRepository.findOneByLogin("user2"));
        Assert.assertEquals(userList.get(0), userRepository.findOneByLogin("user1"));
    }

    @Test
    public void testFindByLoginNegative() throws SQLException {
        Assert.assertNull(userRepository.findOneByLogin("user-test"));
    }

    @Test
    public void testFindByEmail() throws SQLException {
        Assert.assertEquals(userList.get(1), userRepository.findOneByEmail("user@2.ru"));
        Assert.assertEquals(userList.get(0), userRepository.findOneByEmail("user@1.ru"));
    }

    @Test
    public void testFindByEmailNegative() throws SQLException {
        Assert.assertNull(userRepository.findOneByEmail("test@ru"));
    }

    @Test
    public void testIsLoginExist() throws SQLException {
        Assert.assertTrue(userRepository.isLoginExist("user4"));
    }

    @Test
    public void testIsLoginExistNegative() throws SQLException {
        Assert.assertFalse(userRepository.isLoginExist("user4444"));
    }

    @Test
    public void testIsEmailExist() throws SQLException {
        Assert.assertTrue(userRepository.isEmailExist("user@4.ru"));
    }

    @Test
    public void testIsEmailExistNegative() throws SQLException {
        Assert.assertFalse(userRepository.isEmailExist("user4444@.ru"));
    }

}
