package ru.t1.volkova.tm.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.repository.ISessionRepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.model.Session;
import ru.t1.volkova.tm.repository.SessionRepository;
import ru.t1.volkova.tm.service.ConnectionService;
import ru.t1.volkova.tm.service.PropertyService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 3;

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private List<String> userIdList;

    @NotNull
    private ISessionRepository sessionRepository;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Before
    public void initRepository() throws SQLException {
        sessionList = new ArrayList<>();
        userIdList = new ArrayList<>();
        sessionRepository = new SessionRepository(connectionService.getConnection());
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull Session session = new Session();
            session.setRole(Role.USUAL);
            @NotNull String userId = UUID.randomUUID().toString();
            userIdList.add(userId);
            session.setUserId(userId);
            sessionRepository.add(session);
            sessionList.add(session);
        }
    }

    @Test
    public void testAddSession() throws SQLException {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Session newSession = new Session();
        sessionRepository.add(newSession);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testAddAll() throws SQLException {
        final int recordsCount = 3;
        final Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + recordsCount;
        @NotNull final List<Session> sessionList = new ArrayList<>();
        for (int i = 0; i < recordsCount; i++) {
            sessionList.add(new Session());
        }
        sessionRepository.add(sessionList);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final List<Session> sessionList = sessionRepository.findAll();
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionList.size());
    }

    @Test
    public void testFindAllForUser() throws SQLException {
        @NotNull final String userId1 = userIdList.get(0);
        @NotNull final String userId2 = userIdList.get(2);
        @Nullable final List<Session> sessionList = sessionRepository.findAll(userId1);
        @Nullable final List<Session> sessionList2 = sessionRepository.findAll(userId2);
        if (sessionList != null && sessionList2 != null) {
            Assert.assertEquals(sessionRepository.getSize(userId1), sessionList.size());
            Assert.assertEquals(sessionRepository.getSize(userId2), sessionList2.size());
        }
    }

    @Test
    public void testFindAllForUserNegative() throws SQLException {
        @Nullable final List<Session> sessionList = sessionRepository.findAll((String) null);
        Assert.assertNull(sessionList);
        @Nullable final List<Session> sessionList2 = sessionRepository.findAll("non-existent-id");
        if (sessionList2 == null) return;
        Assert.assertEquals(0, sessionList2.size());
    }

    @Test
    public void testFindOneById() throws Exception {
        @Nullable final String sessionId1 = sessionRepository.findOneByIndex(1).getId();
        @Nullable final String sessionId2 = sessionRepository.findOneByIndex(2).getId();
        @NotNull final Session expected1 = sessionList.get(1);
        @NotNull final Session expected2 = sessionList.get(2);
        Assert.assertEquals(expected1, sessionRepository.findOneById(sessionId1));
        Assert.assertEquals(expected2, sessionRepository.findOneById(sessionId2));
    }

    @Test
    public void testFindOneByIdNegative() throws Exception {
        Assert.assertNull(sessionRepository.findOneById("NotExcitingId"));
    }

    @Test
    public void testFindOneByIdForUser() throws SQLException {
        @NotNull final String userId1 = userIdList.get(0);
        @NotNull final String userId2 = userIdList.get(2);
        @NotNull final String id = sessionList.get(0).getId();
        @NotNull final String id2 = sessionList.get(2).getId();
        Assert.assertEquals(sessionList.get(0), sessionRepository.findOneById(userId1, id));
        Assert.assertEquals(sessionList.get(2), sessionRepository.findOneById(userId2, id2));
    }

    @Test
    public void testFindOneByIdForUserNegative() throws SQLException {
        @NotNull final String userId1 = userIdList.get(0);
        @NotNull final String userId2 = userIdList.get(2);
        Assert.assertNull(sessionRepository.findOneById(null, sessionList.get(1).getId()));
        Assert.assertNull(sessionRepository.findOneById(userId1, "NotExcitingId"));
        Assert.assertNull(sessionRepository.findOneById(userId2, "NotExcitingId"));
    }

    @Test
    public void testFindOneByIndex() throws Exception {
        @Nullable final Session session1 = sessionRepository.findOneByIndex(1);
        @Nullable final Session session2 = sessionRepository.findOneByIndex(2);
        @NotNull final Session expected1 = sessionList.get(1);
        @NotNull final Session expected2 = sessionList.get(2);
        Assert.assertEquals(expected1, session1);
        Assert.assertEquals(expected2, session2);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testFindOneByIndexNegative() throws Exception {
        Assert.assertNotNull(sessionRepository.findOneByIndex(NUMBER_OF_ENTRIES + 20));
        Assert.assertNotNull(sessionRepository.findOneByIndex(NUMBER_OF_ENTRIES * 2));
    }

    @Test
    public void testFindOneByIndexForUser() throws Exception {
        @Nullable final Session session1 = sessionRepository.findOneByIndex(1);
        @Nullable final Session session2 = sessionRepository.findOneByIndex(2);
        @NotNull final Session expected1 = sessionList.get(1);
        @NotNull final Session expected2 = sessionList.get(2);
        Assert.assertEquals(expected1, session1);
        Assert.assertEquals(expected2, session2);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testFindOneByIndexForUserNegative() throws Exception {
        Assert.assertNotNull(sessionRepository.findOneByIndex(NUMBER_OF_ENTRIES+20));
        Assert.assertNotNull(sessionRepository.findOneByIndex(NUMBER_OF_ENTRIES*2));
    }

    @Test
    public void testRemoveOne() throws Exception {
        @Nullable final Session session1 = sessionRepository.findOneByIndex(1);
        @Nullable final Session session2 = sessionRepository.findOneByIndex(0);
        Assert.assertEquals(sessionList.get(1),sessionRepository.removeOne(session1));
        Assert.assertEquals(sessionList.get(0),sessionRepository.removeOne(session2));
    }

    @Test
    public void testRemoveOneNegative() throws Exception {
        @NotNull final Session session1 = new Session();
        @NotNull final Session session2 = new Session();
        Assert.assertNull(sessionRepository.removeOne(session1));
        Assert.assertNull(sessionRepository.removeOne(session2));
    }

    @Test
    public void testRemoveOneForUser() throws Exception {
        @NotNull final String userId1 = userIdList.get(0);
        @NotNull final String userId2 = userIdList.get(2);
        @Nullable final Session session1 = sessionRepository.findOneByIndex(0);
        @Nullable final Session session2 = sessionRepository.findOneByIndex(2);
        Assert.assertEquals(sessionList.get(0),sessionRepository.removeOne(userId1, session1));
        Assert.assertEquals(sessionList.get(2),sessionRepository.removeOne(userId2, session2));
    }

    @Test
    public void testRemoveOneForUserNegative() throws SQLException {
        @NotNull final String userId1 = userIdList.get(0);
        @NotNull final String userId2 = userIdList.get(2);
        @NotNull final Session session1 = new Session();
        @NotNull final Session session2 = new Session();
        Assert.assertNull(sessionRepository.removeOne(userId1, session1));
        Assert.assertNull(sessionRepository.removeOne(userId2, session2));
    }

    @Test
    public void testRemoveOneById() throws Exception {
        @Nullable final String sessionId1 = sessionRepository.findOneByIndex(2).getId();
        @Nullable final String sessionId2 = sessionRepository.findOneByIndex(1).getId();
        @NotNull final Session expected1 = sessionList.get(2);
        @NotNull final Session expected2 = sessionList.get(1);
        Assert.assertEquals(expected1, sessionRepository.removeOneById(sessionId1));
        Assert.assertEquals(expected2, sessionRepository.removeOneById(sessionId2));

        Assert.assertNotEquals(expected2, sessionRepository.removeOneById(sessionId1));
        Assert.assertNotEquals(expected1, sessionRepository.removeOneById(sessionId2));
    }

    @Test
    public void testRemoveOneByIdNegative() throws Exception {
        @NotNull final String sessionId1 = UUID.randomUUID().toString();
        @NotNull final String sessionId2 = UUID.randomUUID().toString();
        Assert.assertNull(sessionRepository.removeOneById(sessionId1));
        Assert.assertNull(sessionRepository.removeOneById(sessionId2));
    }

    @Test
    public void testRemoveOneByIdForUser() throws SQLException {
        @NotNull final String userId1 = userIdList.get(0);
        @NotNull final String userId2 = userIdList.get(2);
        @NotNull final String id = sessionList.get(0).getId();
        @NotNull final String id2 = sessionList.get(2).getId();
        Assert.assertEquals(sessionList.get(0), sessionRepository.removeOneById(userId1, id));
        Assert.assertEquals(sessionList.get(2), sessionRepository.removeOneById(userId2, id2));
    }

    @Test
    public void testRemoveOneByIdForUserNegative() throws SQLException {
        @NotNull final String userId1 = userIdList.get(0);
        @NotNull final String userId2 = userIdList.get(2);
        @NotNull final String sessionId1 = UUID.randomUUID().toString();
        @NotNull final String sessionId2 = UUID.randomUUID().toString();
        Assert.assertNull(sessionRepository.removeOneById(userId1, sessionId1));
        Assert.assertNull(sessionRepository.removeOneById(userId2, sessionId2));
        Assert.assertNull(sessionRepository.removeOneById("NotExcitingId", sessionId1));
        Assert.assertNull(sessionRepository.removeOneById(null, sessionId2));
    }

    @Test
    public void testRemoveAll() throws SQLException {
        sessionRepository.removeAll();
        Assert.assertEquals(0, sessionRepository.getSize().intValue());
    }

    @Test
    public void testRemoveAllForUser() throws SQLException {
        @NotNull final String userId1 = userIdList.get(0);
        @NotNull final String userId2 = userIdList.get(1);
        sessionRepository.removeAll(userId1);
        sessionRepository.removeAll(userId2);
        Assert.assertEquals(0, sessionRepository.getSize(userId1));
        Assert.assertEquals(0, sessionRepository.getSize(userId2));
    }

    @Test
    public void testRemoveAllForUserNegative() throws SQLException {
        @NotNull final String userId1 = userIdList.get(2);
        @NotNull final String userId2 = userIdList.get(1);
        sessionRepository.removeAll("NotExcitingId");
        sessionRepository.removeAll((String) null);
        Assert.assertNotEquals(0, sessionRepository.getSize(userId1));
        Assert.assertNotEquals(0, sessionRepository.getSize(userId2));
    }

    @Test
    public void testRemoveOneByIndex() throws Exception {
        @Nullable final Session session1 = sessionRepository.removeOneByIndex(1);
        Assert.assertEquals(NUMBER_OF_ENTRIES-1, sessionRepository.getSize().intValue());
        @Nullable final Session session2 = sessionRepository.removeOneByIndex(0);
        Assert.assertEquals(NUMBER_OF_ENTRIES-2, sessionRepository.getSize().intValue());

        if (session1 != null && session2 != null) {
            Assert.assertNull(sessionRepository.findOneById(session1.getId()));
            Assert.assertNull(sessionRepository.findOneById(session2.getId()));
        }
    }

    @Test
    public void testRemoveOneByIndexNegative() throws Exception {
        Assert.assertNull(sessionRepository.removeOneByIndex(null));
    }

    @Test
    public void testRemoveOneByIndexForUser() throws Exception {
        @NotNull final String userId1 = userIdList.get(2);
        @NotNull final String userId2 = userIdList.get(1);
        @Nullable final Session session1 = sessionRepository.removeOneByIndex(userId1, 0);
        Assert.assertEquals(NUMBER_OF_ENTRIES-1, sessionRepository.getSize().intValue());
        @Nullable final Session session2 = sessionRepository.removeOneByIndex(userId2, 0);
        Assert.assertEquals(NUMBER_OF_ENTRIES-2, sessionRepository.getSize().intValue());

        if (session1 != null && session2 != null) {
            Assert.assertNull(sessionRepository.findOneById(session1.getId()));
            Assert.assertNull(sessionRepository.findOneById(session2.getId()));
        }
    }

    @Test
    public void testRemoveOneByIndexForUserNegative() throws SQLException {
        @NotNull final String userId1 = userIdList.get(2);
        @NotNull final String userId2 = userIdList.get(1);
        Assert.assertNull(sessionRepository.removeOneByIndex(userId1, 77));
        Assert.assertNull(sessionRepository.removeOneByIndex(userId2, 20));
        Assert.assertNull(sessionRepository.removeOneByIndex("NotExcitingId", 2));
        Assert.assertNull(sessionRepository.removeOneByIndex(null, 3));
    }

    @Test
    public void testGetSize() throws SQLException {
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize().intValue());
        Assert.assertEquals(sessionList.size(), sessionRepository.getSize().intValue());
    }

    @Test
    public void testGetSizeForUser() throws SQLException {
        @NotNull final String userId1 = userIdList.get(2);
        @NotNull final String userId2 = userIdList.get(1);
        Assert.assertNotEquals(0,sessionRepository.getSize(userId1));
        Assert.assertNotEquals(0,sessionRepository.getSize(userId2));
    }

    @Test
    public void testExistsById() throws SQLException {
        @NotNull final String userId1 = userIdList.get(2);
        @NotNull final String userId2 = userIdList.get(1);
        Assert.assertTrue(sessionRepository.existsById(userId1, sessionList.get(2).getId()));
        Assert.assertTrue(sessionRepository.existsById(userId2, sessionList.get(1).getId()));
    }

    @Test
    public void testExistsByIdNegative() throws SQLException {
        @NotNull final String userId1 = UUID.randomUUID().toString();
        @NotNull final String userId2 = UUID.randomUUID().toString();
        Assert.assertFalse(sessionRepository.existsById(userId1, sessionList.get(2).getId()));
        Assert.assertFalse(sessionRepository.existsById(userId2, sessionList.get(1).getId()));
    }

}
