package ru.t1.volkova.tm.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.repository.IProjectRepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.enumerated.ProjectSort;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.repository.ProjectRepository;
import ru.t1.volkova.tm.service.ConnectionService;
import ru.t1.volkova.tm.service.PropertyService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Before
    public void initRepository() throws SQLException {
        projectList = new ArrayList<>();
        projectRepository = new ProjectRepository(connectionService.getConnection());
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull Project project = new Project();
            project.setName("Rproject" + i);
            project.setDescription("description" + i);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectRepository.add(project);
            projectList.add(project);
        }
    }

    @Test
    public void testAddProject() throws SQLException {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final Project newProject = new Project();
        projectRepository.add(newProject);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testAddForUserId() throws SQLException {
        Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final String name = "Test project";
        @NotNull final String description = "Test description";
        @NotNull Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(USER_ID_1, project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        @Nullable final Project actualProject = projectRepository.findOneById(USER_ID_1, project.getId());
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(name, actualProject.getName());
        Assert.assertEquals(description, actualProject.getDescription());
        Assert.assertEquals(USER_ID_1, actualProject.getUserId());
    }

    @Test
    public void testAddAll() throws SQLException {
        final int recordsCount = NUMBER_OF_ENTRIES / 2;
        final Integer expectedNumberOfEntries = NUMBER_OF_ENTRIES + recordsCount;
        @NotNull final List<Project> projectList = new ArrayList<>();
        for (int i = 0; i < recordsCount; i++) {
            projectList.add(new Project());
        }
        projectRepository.add(projectList);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testSet() throws Exception {
        final int recordsCount = NUMBER_OF_ENTRIES / 2;
        final Integer expectedNumberOfEntries = recordsCount;
        final int oldProjectSize = projectRepository.findAll().size();
        @NotNull final List<Project> projectList = new ArrayList<>();
        for (int i = 0; i < recordsCount; i++) {
            projectList.add(new Project());
        }
        projectRepository.set(projectList);
        final int newProjectSize = projectRepository.findAll().size();
        Assert.assertNotEquals(newProjectSize, oldProjectSize);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final List<Project> projectList = projectRepository.findAll();
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectList.size());
    }

    @Test
    public void testFindAllWithComparator() throws Exception {
        @NotNull final String sortType = "BY_NAME";
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        if (sort != null) {
            @NotNull final List<Project> projectList = projectRepository.findAll(sort.getComparator());
            Assert.assertEquals(sortType, ProjectSort.BY_NAME.toString());
            Assert.assertEquals(NUMBER_OF_ENTRIES, projectList.size());
        }
    }

    @Test
    public void testFindAllForUser() throws SQLException {
        @Nullable final List<Project> projectList = projectRepository.findAll(USER_ID_1);
        @Nullable final List<Project> projectList2 = projectRepository.findAll(USER_ID_2);
        if (projectList != null && projectList2 != null) {
            Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectList.size());
            Assert.assertEquals(NUMBER_OF_ENTRIES / 2 - 1, projectList2.size());
        }
    }

    @Test
    public void testFindAllForUserNegative() throws SQLException {
        @Nullable final List<Project> projectList = projectRepository.findAll((String) null);
        Assert.assertNull(projectList);
        @Nullable final List<Project> projectList2 = projectRepository.findAll("non-existent-id");
        if (projectList2 == null) return;
        Assert.assertEquals(0, projectList2.size());
    }

    @Test
    public void testFindAllWithComparatorForUser() throws SQLException {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        if (sort != null) {
            @Nullable final List<Project> projectList = projectRepository.findAll(USER_ID_1, sort.getComparator());
            Assert.assertEquals(sortType, ProjectSort.BY_STATUS.toString());
            if (projectList == null) return;
            Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectList.size());
        }
    }

    @Test
    public void testFindAllWithComparatorForUserNegative() throws SQLException {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        if (sort != null) {
            @Nullable final List<Project> projectList = projectRepository.findAll("non-existent-id", sort.getComparator());
            Assert.assertNotEquals(sortType, ProjectSort.BY_NAME.toString());
            if (projectList == null) return;
            Assert.assertEquals(0, projectList.size());
        }
    }

    @Test
    public void testFindOneById() throws Exception {
        @Nullable final String projectId1 = projectRepository.findOneByIndex(1).getId();
        @Nullable final String projectId2 = projectRepository.findOneByIndex(2).getId();
        @NotNull final Project expected1 = projectList.get(1);
        @NotNull final Project expected2 = projectList.get(2);
        Assert.assertEquals(expected1, projectRepository.findOneById(projectId1));
        Assert.assertEquals(expected2, projectRepository.findOneById(projectId2));
    }

    @Test
    public void testFindOneByIdNegative() throws Exception {
        Assert.assertNull(projectRepository.findOneById("NotExcitingId"));
    }

    @Test
    public void testFindOneByIdForUser() throws SQLException {
        for (int i = 0; i < NUMBER_OF_ENTRIES / 2 + 1; i++) {
            @NotNull final String id = projectList.get(i).getId();
            Assert.assertEquals(projectList.get(i), projectRepository.findOneById(USER_ID_1, id));
        }
        for (int i = NUMBER_OF_ENTRIES / 2 + 1; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final String id = projectList.get(i).getId();
            Assert.assertEquals(projectList.get(i), projectRepository.findOneById(USER_ID_2, id));
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() throws SQLException {
        Assert.assertNull(projectRepository.findOneById(null, projectList.get(1).getId()));
        Assert.assertNull(projectRepository.findOneById(USER_ID_1, "NotExcitingId"));
        Assert.assertNull(projectRepository.findOneById(USER_ID_2, "NotExcitingId"));
    }

    @Test
    public void testFindOneByIndex() throws Exception {
        @Nullable final Project project1 = projectRepository.findOneByIndex(1);
        @Nullable final Project project2 = projectRepository.findOneByIndex(2);
        @NotNull final Project expected1 = projectList.get(1);
        @NotNull final Project expected2 = projectList.get(2);
        Assert.assertEquals(expected1, project1);
        Assert.assertEquals(expected2, project2);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testFindOneByIndexNegative() throws Exception {
        Assert.assertNotNull(projectRepository.findOneByIndex(NUMBER_OF_ENTRIES + 20));
        Assert.assertNotNull(projectRepository.findOneByIndex(NUMBER_OF_ENTRIES * 2));
    }

    @Test
    public void testFindOneByIndexForUser() throws Exception {
        @Nullable final Project project1 = projectRepository.findOneByIndex(1);
        @Nullable final Project project2 = projectRepository.findOneByIndex(2);
        @NotNull final Project expected1 = projectList.get(1);
        @NotNull final Project expected2 = projectList.get(2);
        Assert.assertEquals(expected1, project1);
        Assert.assertEquals(expected2, project2);

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testFindOneByIndexForUserNegative() throws Exception {
        Assert.assertNotNull(projectRepository.findOneByIndex(NUMBER_OF_ENTRIES+20));
        Assert.assertNotNull(projectRepository.findOneByIndex(NUMBER_OF_ENTRIES*2));
    }

    @Test
    public void testRemoveOne() throws Exception {
        @Nullable final Project project1 = projectRepository.findOneByIndex(3);
        @Nullable final Project project2 = projectRepository.findOneByIndex(7);
        Assert.assertEquals(projectList.get(3),projectRepository.removeOne(project1));
        Assert.assertEquals(projectList.get(7),projectRepository.removeOne(project2));
    }

    @Test
    public void testRemoveOneNegative() throws Exception {
        @NotNull final Project project1 = new Project();
        @NotNull final Project project2 = new Project();
        Assert.assertNull(projectRepository.removeOne(project1));
        Assert.assertNull(projectRepository.removeOne(project2));
    }

    @Test
    public void testRemoveOneForUser() throws Exception {
        @Nullable final Project project1 = projectRepository.findOneByIndex(3);
        @Nullable final Project project2 = projectRepository.findOneByIndex(7);
        Assert.assertEquals(projectList.get(3),projectRepository.removeOne(USER_ID_1, project1));
        Assert.assertEquals(projectList.get(7),projectRepository.removeOne(USER_ID_2, project2));
    }

    @Test
    public void testRemoveOneForUserNegative() throws SQLException {
        @NotNull final Project project1 = new Project();
        @NotNull final Project project2 = new Project();
        Assert.assertNull(projectRepository.removeOne(USER_ID_1, project1));
        Assert.assertNull(projectRepository.removeOne(USER_ID_2, project2));
    }

    @Test
    public void testRemoveOneById() throws Exception {
        @Nullable final String projectId1 = projectRepository.findOneByIndex(4).getId();
        @Nullable final String projectId2 = projectRepository.findOneByIndex(3).getId();
        @NotNull final Project expected1 = projectList.get(4);
        @NotNull final Project expected2 = projectList.get(3);
        Assert.assertEquals(expected1, projectRepository.removeOneById(projectId1));
        Assert.assertEquals(expected2, projectRepository.removeOneById(projectId2));

        Assert.assertNotEquals(expected2, projectRepository.removeOneById(projectId1));
        Assert.assertNotEquals(expected1, projectRepository.removeOneById(projectId2));
    }

    @Test
    public void testRemoveOneByIdNegative() throws Exception {
        @NotNull final String projectId1 = UUID.randomUUID().toString();
        @NotNull final String projectId2 = UUID.randomUUID().toString();
        Assert.assertNull(projectRepository.removeOneById(projectId1));
        Assert.assertNull(projectRepository.removeOneById(projectId2));
    }

    @Test
    public void testRemoveOneByIdForUser() throws SQLException {
        for (int i = 0; i < NUMBER_OF_ENTRIES / 2 + 1; i++) {
            @NotNull final String id = projectList.get(i).getId();
            Assert.assertEquals(projectList.get(i), projectRepository.removeOneById(USER_ID_1, id));
        }
        for (int i = NUMBER_OF_ENTRIES / 2 + 1; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final String id = projectList.get(i).getId();
            Assert.assertEquals(projectList.get(i), projectRepository.removeOneById(USER_ID_2, id));
        }
    }

    @Test
    public void testRemoveOneByIdForUserNegative() throws SQLException {
        @NotNull final String projectId1 = UUID.randomUUID().toString();
        @NotNull final String projectId2 = UUID.randomUUID().toString();
        Assert.assertNull(projectRepository.removeOneById(USER_ID_1, projectId1));
        Assert.assertNull(projectRepository.removeOneById(USER_ID_2, projectId2));
        Assert.assertNull(projectRepository.removeOneById("NotExcitingId", projectId1));
        Assert.assertNull(projectRepository.removeOneById(null, projectId2));
    }

    @Test
    public void testRemoveAll() throws SQLException {
        projectRepository.removeAll();
        Assert.assertEquals(0, projectRepository.getSize().intValue());
    }

    @Test
    public void testRemoveAllForUser() throws SQLException {
        projectRepository.removeAll(USER_ID_1);
        projectRepository.removeAll(USER_ID_2);
        Assert.assertEquals(0, projectRepository.getSize(USER_ID_1));
        Assert.assertEquals(0, projectRepository.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveAllForUserNegative() throws SQLException {
        projectRepository.removeAll("NotExcitingId");
        projectRepository.removeAll((String) null);
        Assert.assertNotEquals(0, projectRepository.getSize(USER_ID_1));
        Assert.assertNotEquals(0, projectRepository.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveOneByIndex() throws Exception {
        @Nullable final Project project1 = projectRepository.removeOneByIndex(1);
        Assert.assertEquals(NUMBER_OF_ENTRIES-1, projectRepository.getSize().intValue());
        @Nullable final Project project2 = projectRepository.removeOneByIndex(5);
        Assert.assertEquals(NUMBER_OF_ENTRIES-2, projectRepository.getSize().intValue());

        if (project1 != null && project2 != null) {
            Assert.assertNull(projectRepository.findOneById(project1.getId()));
            Assert.assertNull(projectRepository.findOneById(project2.getId()));
        }
    }

    @Test
    public void testRemoveOneByIndexNegative() throws Exception {
        Assert.assertNull(projectRepository.removeOneByIndex(null));
    }

    @Test
    public void testRemoveOneByIndexForUser() throws Exception {
        @Nullable final Project project1 = projectRepository.removeOneByIndex(USER_ID_1, 1);
        Assert.assertEquals(NUMBER_OF_ENTRIES-1, projectRepository.getSize().intValue());
        @Nullable final Project project2 = projectRepository.removeOneByIndex(USER_ID_2, 2);
        Assert.assertEquals(NUMBER_OF_ENTRIES-2, projectRepository.getSize().intValue());

        if (project1 != null && project2 != null) {
            Assert.assertNull(projectRepository.findOneById(project1.getId()));
            Assert.assertNull(projectRepository.findOneById(project2.getId()));
        }
    }

    @Test
    public void testRemoveOneByIndexForUserNegative() throws SQLException {
        Assert.assertNull(projectRepository.removeOneByIndex(USER_ID_1, 77));
        Assert.assertNull(projectRepository.removeOneByIndex(USER_ID_2, 20));
        Assert.assertNull(projectRepository.removeOneByIndex("NotExcitingId", 2));
        Assert.assertNull(projectRepository.removeOneByIndex(null, 3));
    }


    @Test
    public void testGetSize() throws SQLException {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize().intValue());
        Assert.assertEquals(projectList.size(), projectRepository.getSize().intValue());
    }

    @Test
    public void testGetSizeForUser() throws SQLException {
        Assert.assertEquals(NUMBER_OF_ENTRIES/2+1, projectRepository.getSize(USER_ID_1));
        Assert.assertEquals(NUMBER_OF_ENTRIES/2-1, projectRepository.getSize(USER_ID_2));
        Assert.assertEquals(projectList.size()/2+1, projectRepository.getSize(USER_ID_1));
        Assert.assertEquals(projectList.size()/2-1, projectRepository.getSize(USER_ID_2));
    }

    @Test
    public void testExistsById() throws SQLException {
        for (int i = 0; i < NUMBER_OF_ENTRIES/2+1; i++) {
            @NotNull final String id = projectList.get(i).getId();
            Assert.assertTrue(projectRepository.existsById(USER_ID_1, id));
        }
        for (int i = NUMBER_OF_ENTRIES/2+1; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final String id = projectList.get(i).getId();
            Assert.assertTrue(projectRepository.existsById(USER_ID_2, id));
        }
    }

    @Test
    public void testExistsByIdNegative() throws SQLException {
        @NotNull final String userId1 = UUID.randomUUID().toString();
        @NotNull final String userId2 = UUID.randomUUID().toString();
        for (int i = 0; i < NUMBER_OF_ENTRIES/2+1; i++) {
            @NotNull final String id = projectList.get(i).getId();
            Assert.assertFalse(projectRepository.existsById(userId1, id));
        }
        for (int i = NUMBER_OF_ENTRIES/2+1; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final String id = projectList.get(i).getId();
            Assert.assertFalse(projectRepository.existsById(userId2, id));
        }
    }

}
